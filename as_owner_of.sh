#!/bin/sh

# This file runs code as the owner of the given folder, creating a matching
# user account if necessary.  We need to do this on Docker Linux (but not
# Docker Mac or Podman) because mapped folders don't get UID mapping, so
# editing files within a mapped folder from a docker container can cause them
# to be owned by root.
#
# Supported distros:
# - debian
# - ubuntu
# - fedora
# - redhat variants with dnf or yum
# - alpine
# - arch
#
# Usage:
# ./as_owner_of.sh --dockerfile [mapped drive]
#     Emit Dockerfile code for installing this script. The [mapped drive] is a
#     path to a VOLUME you expect to be mounted by the calling user.
#
# ./as_owner_of.sh --setup
#     Invoke from a dockerfile to install dependencies. Must be run as root.
#
# ./as_owner_of.sh [filename] [cmd argv...]
#     Invoke the given command argv as the user+group that owns the given file.
#     This should be the ENTRYPOINT of your docker container.
#
# Environment Variables at `docker run` time:
#   FAKE_ACCOUNT
#     Name of the user/group to add if needed to match permissions. Defaults
#     to "hostuser"
#
#   FORCE_ROOT
#     When set to "yes", will prevent this script from doing anything, even if
#     the container is running as root and the mapped drive is owned by someone
#     else.
#
# This File Copyright (c) 2022 drmoose.net and is distributed under the terms
# of the BSD license. Please distribute it widely. Copy & paste it into your
# own source code. Claim it's yours. I don't care. I just want your docker
# containers to stop trashing the permissions on my system.

FAKE_ACCOUNT=${FAKE_ACCOUNT:-hostuser}

# dockerfile mode just emits the code needed to include this script in a
# container
if [ r"$1" = r"--dockerfile" ]; then
	if [ -z "$2" ]; then
		echo "Usage: $0 --dockerfile [mapped drive path]" >&2
		exit 1
	fi
	cat <<EOF
ADD $(basename "$0") /sbin/as_owner_of.sh
RUN /sbin/as_owner_of.sh --setup
ENTRYPOINT ["/sbin/as_owner_of.sh", "$2"]
EOF
	exit 0
fi

# Setup mode is called by the enclosing dockerfile to install dependencies.
if [ r"$1" = r"--setup" ]; then
	. /etc/os-release  # Included in most docker distros. Sets ID=
	case "$ID $ID_LIKE" in
		*debian*)
			set -xe
			export DEBIAN_FRONTEND=noninteractive
			apt-get update
			apt-get install -y sudo
			rm -rf /var/lib/apt/lists/*
			;;
		*redhat*|*fedora*|*alma*|*rocky*|*cent*)
			if hash dnf 2>/dev/null; then
				installer=dnf
			elif hash yum 2>/dev/null; then
				installer=yum
			else
				echo "Don't know installer binary for $ID" >&2
				exit 1
			fi
			set -xe
			$installer install -y sudo
			;;
		*alpine*)
			set -xe
			apk add sudo
			;;
		*arch*)
			set -xe
			pacman -S sudo
			;;
	esac
	# Check for the existence of programs this script depends on, so failures
	# happen at build-time rather than runtime
	hash sudo
	hash stat
	hash id
	hash groupadd || hash addgroup
	hash useradd || hash adduser
	hash exec
	exit 0
fi

exemplar="$1"
shift

add_sudoer() {
	printf '\n%s ALL=(ALL:ALL) NOPASSWD: ALL\n' "$1" >>/etc/sudoers
}

begin() {
	exec "$@"
}

cur_uid="$(id -ru)"
cur_gid="$(id -rg)"
if [ r"$cur_uid" != r"0" -o r"$FORCE_ROOT" != r"yes" ]; then
	if [ -e "$exemplar" ]; then
		target_uid=$(stat -c %u "$exemplar")
		target_uname=$(stat -c %U "$exemplar")
		target_gid=$(stat -c %g "$exemplar")
		target_gname=$(stat -c %G "$exemplar")
	else
		echo "$exemplar" does not exist. Cannot inherit its permissions. >&2
		exit 21
	fi

	if [ r"$target_gname" = r"UNKNOWN" ]; then
		if hash groupadd 2>/dev/null; then
			groupadd -g "$target_gid" "$FAKE_ACCOUNT"
		elif hash addgroup 2>/dev/null; then
			# alpine has unusual flags for addgroup/adduser but is also the only
			# supported distro that doesn't have groupadd/useradd
			addgroup -g "$target_gid" "$FAKE_ACCOUNT"
		else
			echo "No addgroup/groupadd script seems to be present on this system." >&2
			exit 22
		fi
		target_gname="$FAKE_ACCOUNT"
	fi

	if [ r"$target_uname" = r"UNKNOWN" ]; then
		if hash useradd 2>/dev/null; then
			useradd -g "$target_gid" -u "$target_uid" "$FAKE_ACCOUNT"
		elif hash adduser 2>/dev/null; then
			# alpine has unusual flags for addgroup/adduser but is also the only
			# supported distro that doesn't have groupadd/useradd
			adduser -G "$target_gname" -D -u "$target_uid" "$FAKE_ACCOUNT"
		else
			echo "No adduser/useradd script seems to be present on this system." >&2
			exit 23
		fi
		# Set password to something nonempty (some distros need this to "activate"
		# the account)
		printf 'xxx\nxxx\n' | passwd "$FAKE_ACCOUNT" >/dev/null 2>/dev/null

		# Add the user to sudoers
		add_sudoer "$FAKE_ACCOUNT"
		target_uname="$FAKE_ACCOUNT"
	fi

	# Add self to sudoers
	add_sudoer "$(id -ru)"

	# Use sudo to launch rather than launching directly
	if [ r"$target_gid" != r"$cur_gid" -o r"$target_uid" != r"$cur_uid" ]; then
		begin() {
			exec sudo -g "$target_gname" -u "$target_uname" -- "$@"
		}
	fi
fi

if [ -z "$*" ]; then
	# No CMD given so let's try to find a shell
	if [ -x /bin/bash ]; then
		begin /bin/bash
	else
		begin /bin/sh
	fi
else
	begin "$@"
fi
